<img src="https://gitee.com/duobab/duobab/raw/master/res/images/duo.jpg" height="100" width="200">

[![license](https://img.shields.io/github/license/seata/seata.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

[duobab.com](http://www.duobab.com) 

## 简介

基于spring data jpa 操作postgreSql数据库使用最简demo
- 拉取代码
- 启动 mysql, 并创建一个数据库, 可在application.properties更新你自己的数据库连接信息 , 数据库中的表无需创建, 程序启动会自动创建
- 运行SpringbootJpaPostgresqlDemoApplication类中的main方法
- 本地浏览器访问 http://localhost ,即可进行简单的CRUD操作
