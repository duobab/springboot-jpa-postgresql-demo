package com.duobab.springbootjpapostgresqldemo.dao;

import com.duobab.springbootjpapostgresqldemo.domain.WebSite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WebSiteRepository extends JpaRepository<WebSite, Long> {

}
