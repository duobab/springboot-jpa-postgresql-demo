package com.duobab.springbootjpapostgresqldemo.controller.result;

import lombok.Data;

@Data
public class WebSiteResult {
    private Long id;
    private String url;
}
