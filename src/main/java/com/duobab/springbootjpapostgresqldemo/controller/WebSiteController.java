package com.duobab.springbootjpapostgresqldemo.controller;

import com.duobab.springbootjpapostgresqldemo.controller.param.WebSiteParam;
import com.duobab.springbootjpapostgresqldemo.service.WebSiteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/website")
public class WebSiteController extends AbstractController {
    @Autowired
    private WebSiteServiceImpl webSiteServiceImpl;

    @RequestMapping(method = RequestMethod.POST)
    public void addWebSite(@RequestBody WebSiteParam webSiteParam) {
        webSiteServiceImpl.addWebSite(webSiteParam);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delWebSite(@PathVariable("id") Long id) {
        webSiteServiceImpl.delWebSite(id);
    }
}
