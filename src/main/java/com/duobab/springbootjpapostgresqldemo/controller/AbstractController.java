package com.duobab.springbootjpapostgresqldemo.controller;

import com.duobab.springbootjpapostgresqldemo.controller.result.WebSiteResult;
import com.duobab.springbootjpapostgresqldemo.service.WebSiteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import java.util.List;

public abstract class AbstractController {
    @Autowired
    private WebSiteServiceImpl webSiteServiceImpl;

    public void loadData(Model model) {
        List<WebSiteResult> data = webSiteServiceImpl.getWebSite();
        model.addAttribute("data", data);
    }
}
