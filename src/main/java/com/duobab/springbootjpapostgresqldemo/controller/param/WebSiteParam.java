package com.duobab.springbootjpapostgresqldemo.controller.param;

import lombok.Data;

@Data
public class WebSiteParam {
    private String url;
}
