package com.duobab.springbootjpapostgresqldemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller()
@RequestMapping("/")
public class IndexController extends AbstractController {

    @RequestMapping(method = RequestMethod.GET)
    public String getIndex(Model model) {
        loadData(model);
        return "index";
    }
}
